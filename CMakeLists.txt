cmake_minimum_required (VERSION 3.0.0)

PROJECT(kcd_diceGame VERSION 0.0.1)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON) #For you complete me (vim)

add_compile_options(-Wall -Wextra -pedantic -Werror)

include(FetchContent)
FetchContent_Declare(
  googletest
  URL https://github.com/google/googletest/archive/e2239ee6043f73722e7aa812a459f54a28552929.zip
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

enable_testing()

add_subdirectory(src)
add_subdirectory(tests)

#For debian package creation using CPack
install(TARGETS kcd_diceGame
    RUNTIME DESTINATION "/usr/bin"
    LIBRARY DESTINATION "/usr/saa/"
    DESTINATION "/usr/saa/"
)

SET(CPACK_GENERATOR "DEB")
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "S. Aalhus") 
SET(CPACK_PACKAGE_FILE_NAME kcd_diceGame-${CMAKE_PROJECT_VERSION})
INCLUDE(CPack)
