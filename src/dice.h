#ifndef _DICE_H
#define _DICE_H

#include <cstdint>

class Dice
{
private:
    uint8_t m_sides;
    uint8_t m_value;
public:
    Dice(uint8_t sides);
    uint8_t roll();
    uint8_t getValue();
};

#endif