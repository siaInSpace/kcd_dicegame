#include "dice.h"

Dice::Dice(uint8_t sides) :
m_sides(sides),
m_value(0)
{
    roll();
}

uint8_t Dice::getValue()
{
    return m_value;
}

uint8_t Dice::roll()
{
    m_value = (m_value + 1) % m_sides;
    return m_value;
}